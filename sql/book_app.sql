/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 90514
 Source Host           : 192.168.102.25:5432
 Source Catalog        : book_app
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90514
 File Encoding         : 65001

 Date: 20/12/2018 17:53:33
*/


-- ----------------------------
-- Sequence structure for auth_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_id_seq";
CREATE SEQUENCE "public"."auth_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_group_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_permissions_id_seq";
CREATE SEQUENCE "public"."auth_group_permissions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_permission_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_permission_id_seq";
CREATE SEQUENCE "public"."auth_permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_user_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_groups_id_seq";
CREATE SEQUENCE "public"."auth_user_groups_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_id_seq";
CREATE SEQUENCE "public"."auth_user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_user_user_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_user_permissions_id_seq";
CREATE SEQUENCE "public"."auth_user_user_permissions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for authors_genre_ids_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."authors_genre_ids_id_seq";
CREATE SEQUENCE "public"."authors_genre_ids_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for authors_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."authors_id_seq";
CREATE SEQUENCE "public"."authors_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for books_author_ids_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."books_author_ids_id_seq";
CREATE SEQUENCE "public"."books_author_ids_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for books_genre_ids_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."books_genre_ids_id_seq";
CREATE SEQUENCE "public"."books_genre_ids_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for books_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."books_id_seq";
CREATE SEQUENCE "public"."books_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for comments_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comments_id_seq";
CREATE SEQUENCE "public"."comments_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for django_admin_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_admin_log_id_seq";
CREATE SEQUENCE "public"."django_admin_log_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for django_content_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_content_type_id_seq";
CREATE SEQUENCE "public"."django_content_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for django_migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_migrations_id_seq";
CREATE SEQUENCE "public"."django_migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for editions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."editions_id_seq";
CREATE SEQUENCE "public"."editions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for formalities_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."formalities_id_seq";
CREATE SEQUENCE "public"."formalities_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for genres_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."genres_id_seq";
CREATE SEQUENCE "public"."genres_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for languages_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."languages_id_seq";
CREATE SEQUENCE "public"."languages_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group";
CREATE TABLE "public"."auth_group" (
  "id" int4 NOT NULL DEFAULT nextval('auth_group_id_seq'::regclass),
  "name" varchar(80) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group_permissions";
CREATE TABLE "public"."auth_group_permissions" (
  "id" int4 NOT NULL DEFAULT nextval('auth_group_permissions_id_seq'::regclass),
  "group_id" int4 NOT NULL,
  "permission_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_permission";
CREATE TABLE "public"."auth_permission" (
  "id" int4 NOT NULL DEFAULT nextval('auth_permission_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "content_type_id" int4 NOT NULL,
  "codename" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "public"."auth_permission" VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO "public"."auth_permission" VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO "public"."auth_permission" VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO "public"."auth_permission" VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO "public"."auth_permission" VALUES (5, 'Can add group', 2, 'add_group');
INSERT INTO "public"."auth_permission" VALUES (6, 'Can change group', 2, 'change_group');
INSERT INTO "public"."auth_permission" VALUES (7, 'Can delete group', 2, 'delete_group');
INSERT INTO "public"."auth_permission" VALUES (8, 'Can view group', 2, 'view_group');
INSERT INTO "public"."auth_permission" VALUES (9, 'Can add permission', 3, 'add_permission');
INSERT INTO "public"."auth_permission" VALUES (10, 'Can change permission', 3, 'change_permission');
INSERT INTO "public"."auth_permission" VALUES (11, 'Can delete permission', 3, 'delete_permission');
INSERT INTO "public"."auth_permission" VALUES (12, 'Can view permission', 3, 'view_permission');
INSERT INTO "public"."auth_permission" VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO "public"."auth_permission" VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO "public"."auth_permission" VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO "public"."auth_permission" VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO "public"."auth_permission" VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO "public"."auth_permission" VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO "public"."auth_permission" VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO "public"."auth_permission" VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO "public"."auth_permission" VALUES (21, 'Can add edition', 6, 'add_edition');
INSERT INTO "public"."auth_permission" VALUES (22, 'Can change edition', 6, 'change_edition');
INSERT INTO "public"."auth_permission" VALUES (23, 'Can delete edition', 6, 'delete_edition');
INSERT INTO "public"."auth_permission" VALUES (24, 'Can view edition', 6, 'view_edition');
INSERT INTO "public"."auth_permission" VALUES (25, 'Can add genre', 7, 'add_genre');
INSERT INTO "public"."auth_permission" VALUES (26, 'Can change genre', 7, 'change_genre');
INSERT INTO "public"."auth_permission" VALUES (27, 'Can delete genre', 7, 'delete_genre');
INSERT INTO "public"."auth_permission" VALUES (28, 'Can view genre', 7, 'view_genre');
INSERT INTO "public"."auth_permission" VALUES (29, 'Can add language', 8, 'add_language');
INSERT INTO "public"."auth_permission" VALUES (30, 'Can change language', 8, 'change_language');
INSERT INTO "public"."auth_permission" VALUES (31, 'Can delete language', 8, 'delete_language');
INSERT INTO "public"."auth_permission" VALUES (32, 'Can view language', 8, 'view_language');
INSERT INTO "public"."auth_permission" VALUES (33, 'Can add formality', 9, 'add_formality');
INSERT INTO "public"."auth_permission" VALUES (34, 'Can change formality', 9, 'change_formality');
INSERT INTO "public"."auth_permission" VALUES (35, 'Can delete formality', 9, 'delete_formality');
INSERT INTO "public"."auth_permission" VALUES (36, 'Can view formality', 9, 'view_formality');
INSERT INTO "public"."auth_permission" VALUES (37, 'Can add author', 10, 'add_author');
INSERT INTO "public"."auth_permission" VALUES (38, 'Can change author', 10, 'change_author');
INSERT INTO "public"."auth_permission" VALUES (39, 'Can delete author', 10, 'delete_author');
INSERT INTO "public"."auth_permission" VALUES (40, 'Can view author', 10, 'view_author');
INSERT INTO "public"."auth_permission" VALUES (41, 'Can add book', 11, 'add_book');
INSERT INTO "public"."auth_permission" VALUES (42, 'Can change book', 11, 'change_book');
INSERT INTO "public"."auth_permission" VALUES (43, 'Can delete book', 11, 'delete_book');
INSERT INTO "public"."auth_permission" VALUES (44, 'Can view book', 11, 'view_book');
INSERT INTO "public"."auth_permission" VALUES (45, 'Can add comment', 12, 'add_comment');
INSERT INTO "public"."auth_permission" VALUES (46, 'Can change comment', 12, 'change_comment');
INSERT INTO "public"."auth_permission" VALUES (47, 'Can delete comment', 12, 'delete_comment');
INSERT INTO "public"."auth_permission" VALUES (48, 'Can view comment', 12, 'view_comment');
INSERT INTO "public"."auth_permission" VALUES (49, 'Can add session', 13, 'add_session');
INSERT INTO "public"."auth_permission" VALUES (50, 'Can change session', 13, 'change_session');
INSERT INTO "public"."auth_permission" VALUES (51, 'Can delete session', 13, 'delete_session');
INSERT INTO "public"."auth_permission" VALUES (52, 'Can view session', 13, 'view_session');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user";
CREATE TABLE "public"."auth_user" (
  "id" int4 NOT NULL DEFAULT nextval('auth_user_id_seq'::regclass),
  "password" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "last_login" timestamptz(6),
  "is_superuser" bool NOT NULL,
  "username" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "first_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(254) COLLATE "pg_catalog"."default" NOT NULL,
  "is_staff" bool NOT NULL,
  "is_active" bool NOT NULL,
  "date_joined" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO "public"."auth_user" VALUES (2, 'pbkdf2_sha256$120000$mZ6tHjqVv8Lx$+xHFLKHSNYJ+VzREnC8R+GQHwOU7GXhM+MdbqeRPDPw=', NULL, 'f', 'quang', '', '', '', 'f', 't', '2018-12-19 09:20:57+00');
INSERT INTO "public"."auth_user" VALUES (1, 'pbkdf2_sha256$120000$4oO6kLpxgrO8$a8YP4/6HcRi8ESuacH1gY3YT+9PWXhuLJDsDcCpymEE=', '2018-12-20 10:46:03.325618+00', 't', 'admin', '', '', '', 't', 't', '2018-12-19 08:49:04.743645+00');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user_groups";
CREATE TABLE "public"."auth_user_groups" (
  "id" int4 NOT NULL DEFAULT nextval('auth_user_groups_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "group_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user_user_permissions";
CREATE TABLE "public"."auth_user_user_permissions" (
  "id" int4 NOT NULL DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "permission_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for authors
-- ----------------------------
DROP TABLE IF EXISTS "public"."authors";
CREATE TABLE "public"."authors" (
  "id" int4 NOT NULL DEFAULT nextval('authors_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "image" varchar(100) COLLATE "pg_catalog"."default",
  "dob" int4 NOT NULL,
  "hometown" varchar(150) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of authors
-- ----------------------------
INSERT INTO "public"."authors" VALUES (16, 'Quang Phuoc Dang', '', 1997, 'Vietnam', NULL);

-- ----------------------------
-- Table structure for authors_genre_ids
-- ----------------------------
DROP TABLE IF EXISTS "public"."authors_genre_ids";
CREATE TABLE "public"."authors_genre_ids" (
  "id" int4 NOT NULL DEFAULT nextval('authors_genre_ids_id_seq'::regclass),
  "author_id" int4 NOT NULL,
  "genre_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of authors_genre_ids
-- ----------------------------
INSERT INTO "public"."authors_genre_ids" VALUES (1, 16, 1);
INSERT INTO "public"."authors_genre_ids" VALUES (2, 16, 2);

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS "public"."books";
CREATE TABLE "public"."books" (
  "id" int4 NOT NULL DEFAULT nextval('books_id_seq'::regclass),
  "name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "cover_image" varchar(100) COLLATE "pg_catalog"."default",
  "isbn" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "number_of_pages" int4,
  "publish_year" int4 NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "edition_id" int4,
  "formality_id" int4,
  "language_id" int4
)
;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO "public"."books" VALUES (1, 'Chạy ngay đi', '', '123456121', 123, 2015, NULL, 1, 2, 2);

-- ----------------------------
-- Table structure for books_author_ids
-- ----------------------------
DROP TABLE IF EXISTS "public"."books_author_ids";
CREATE TABLE "public"."books_author_ids" (
  "id" int4 NOT NULL DEFAULT nextval('books_author_ids_id_seq'::regclass),
  "book_id" int4 NOT NULL,
  "author_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of books_author_ids
-- ----------------------------
INSERT INTO "public"."books_author_ids" VALUES (1, 1, 16);

-- ----------------------------
-- Table structure for books_genre_ids
-- ----------------------------
DROP TABLE IF EXISTS "public"."books_genre_ids";
CREATE TABLE "public"."books_genre_ids" (
  "id" int4 NOT NULL DEFAULT nextval('books_genre_ids_id_seq'::regclass),
  "book_id" int4 NOT NULL,
  "genre_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of books_genre_ids
-- ----------------------------
INSERT INTO "public"."books_genre_ids" VALUES (1, 1, 1);
INSERT INTO "public"."books_genre_ids" VALUES (2, 1, 2);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS "public"."comments";
CREATE TABLE "public"."comments" (
  "id" int4 NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
  "content" text COLLATE "pg_catalog"."default",
  "publish_date" date NOT NULL,
  "book_id" int4,
  "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO "public"."comments" VALUES (1, 'Chưa bao giờ và đừng mong chờ.', '2018-12-20', 1, 2);
INSERT INTO "public"."comments" VALUES (2, 'Thật sự rất kinh khủng khiếp', '2018-12-20', 1, 1);

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_admin_log";
CREATE TABLE "public"."django_admin_log" (
  "id" int4 NOT NULL DEFAULT nextval('django_admin_log_id_seq'::regclass),
  "action_time" timestamptz(6) NOT NULL,
  "object_id" text COLLATE "pg_catalog"."default",
  "object_repr" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "action_flag" int2 NOT NULL,
  "change_message" text COLLATE "pg_catalog"."default" NOT NULL,
  "content_type_id" int4,
  "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO "public"."django_admin_log" VALUES (1, '2018-12-19 09:20:57.892277+00', '2', 'quang', 1, '[{"added": {}}]', 4, 1);
INSERT INTO "public"."django_admin_log" VALUES (2, '2018-12-19 09:21:03.033973+00', '2', 'quang', 2, '[]', 4, 1);
INSERT INTO "public"."django_admin_log" VALUES (3, '2018-12-20 10:36:39.471589+00', '1', 'Chưa bao giờ và đừng mong chờ.', 1, '[{"added": {}}]', 12, 1);

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_content_type";
CREATE TABLE "public"."django_content_type" (
  "id" int4 NOT NULL DEFAULT nextval('django_content_type_id_seq'::regclass),
  "app_label" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "model" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "public"."django_content_type" VALUES (1, 'admin', 'logentry');
INSERT INTO "public"."django_content_type" VALUES (2, 'auth', 'group');
INSERT INTO "public"."django_content_type" VALUES (3, 'auth', 'permission');
INSERT INTO "public"."django_content_type" VALUES (4, 'auth', 'user');
INSERT INTO "public"."django_content_type" VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO "public"."django_content_type" VALUES (6, 'core', 'edition');
INSERT INTO "public"."django_content_type" VALUES (7, 'core', 'genre');
INSERT INTO "public"."django_content_type" VALUES (8, 'core', 'language');
INSERT INTO "public"."django_content_type" VALUES (9, 'core', 'formality');
INSERT INTO "public"."django_content_type" VALUES (10, 'core', 'author');
INSERT INTO "public"."django_content_type" VALUES (11, 'core', 'book');
INSERT INTO "public"."django_content_type" VALUES (12, 'core', 'comment');
INSERT INTO "public"."django_content_type" VALUES (13, 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_migrations";
CREATE TABLE "public"."django_migrations" (
  "id" int4 NOT NULL DEFAULT nextval('django_migrations_id_seq'::regclass),
  "app" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "applied" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO "public"."django_migrations" VALUES (1, 'contenttypes', '0001_initial', '2018-12-19 08:46:14.816627+00');
INSERT INTO "public"."django_migrations" VALUES (2, 'auth', '0001_initial', '2018-12-19 08:46:14.936107+00');
INSERT INTO "public"."django_migrations" VALUES (3, 'admin', '0001_initial', '2018-12-19 08:46:14.991596+00');
INSERT INTO "public"."django_migrations" VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2018-12-19 08:46:15.000343+00');
INSERT INTO "public"."django_migrations" VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2018-12-19 08:46:15.012421+00');
INSERT INTO "public"."django_migrations" VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2018-12-19 08:46:15.084871+00');
INSERT INTO "public"."django_migrations" VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2018-12-19 08:46:15.091161+00');
INSERT INTO "public"."django_migrations" VALUES (8, 'auth', '0003_alter_user_email_max_length', '2018-12-19 08:46:15.113043+00');
INSERT INTO "public"."django_migrations" VALUES (9, 'auth', '0004_alter_user_username_opts', '2018-12-19 08:46:15.122378+00');
INSERT INTO "public"."django_migrations" VALUES (10, 'auth', '0005_alter_user_last_login_null', '2018-12-19 08:46:15.17142+00');
INSERT INTO "public"."django_migrations" VALUES (11, 'auth', '0006_require_contenttypes_0002', '2018-12-19 08:46:15.173998+00');
INSERT INTO "public"."django_migrations" VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2018-12-19 08:46:15.183922+00');
INSERT INTO "public"."django_migrations" VALUES (13, 'auth', '0008_alter_user_username_max_length', '2018-12-19 08:46:15.224621+00');
INSERT INTO "public"."django_migrations" VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2018-12-19 08:46:15.236152+00');
INSERT INTO "public"."django_migrations" VALUES (15, 'core', '0001_initial', '2018-12-19 08:46:15.475581+00');
INSERT INTO "public"."django_migrations" VALUES (16, 'sessions', '0001_initial', '2018-12-19 08:56:47.042357+00');
INSERT INTO "public"."django_migrations" VALUES (17, 'core', '0002_auto_20181220_1035', '2018-12-20 10:36:04.683615+00');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_session";
CREATE TABLE "public"."django_session" (
  "session_key" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "session_data" text COLLATE "pg_catalog"."default" NOT NULL,
  "expire_date" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO "public"."django_session" VALUES ('ziayhjz9v2qq2dznrimtwrq0l3oix43j', 'ZjM1YzJlMzdjYWM2OTI3YTJlZWZhZGY1NzlkNDYxZjc1NmI1ZjMwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJmNjE0NDg3YzU2MGFkZTc3MGY1MGE5ODllNWMxNTczMzIxNzVhZTk4In0=', '2019-01-03 10:46:03.327857+00');

-- ----------------------------
-- Table structure for editions
-- ----------------------------
DROP TABLE IF EXISTS "public"."editions";
CREATE TABLE "public"."editions" (
  "id" int4 NOT NULL DEFAULT nextval('editions_id_seq'::regclass),
  "name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of editions
-- ----------------------------
INSERT INTO "public"."editions" VALUES (1, '2015');

-- ----------------------------
-- Table structure for formalities
-- ----------------------------
DROP TABLE IF EXISTS "public"."formalities";
CREATE TABLE "public"."formalities" (
  "id" int4 NOT NULL DEFAULT nextval('formalities_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of formalities
-- ----------------------------
INSERT INTO "public"."formalities" VALUES (2, 'Paperback');
INSERT INTO "public"."formalities" VALUES (1, 'Hardcover');

-- ----------------------------
-- Table structure for genres
-- ----------------------------
DROP TABLE IF EXISTS "public"."genres";
CREATE TABLE "public"."genres" (
  "id" int4 NOT NULL DEFAULT nextval('genres_id_seq'::regclass),
  "name" varchar(45) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "parent_id" int4
)
;

-- ----------------------------
-- Records of genres
-- ----------------------------
INSERT INTO "public"."genres" VALUES (1, 'Science Fiction', NULL, NULL);
INSERT INTO "public"."genres" VALUES (2, 'Fantasy', NULL, NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS "public"."languages";
CREATE TABLE "public"."languages" (
  "id" int4 NOT NULL DEFAULT nextval('languages_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO "public"."languages" VALUES (1, 'English', NULL);
INSERT INTO "public"."languages" VALUES (2, 'Vietnamese', NULL);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."auth_group_id_seq"
OWNED BY "public"."auth_group"."id";
SELECT setval('"public"."auth_group_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_group_permissions_id_seq"
OWNED BY "public"."auth_group_permissions"."id";
SELECT setval('"public"."auth_group_permissions_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_permission_id_seq"
OWNED BY "public"."auth_permission"."id";
SELECT setval('"public"."auth_permission_id_seq"', 53, true);
ALTER SEQUENCE "public"."auth_user_groups_id_seq"
OWNED BY "public"."auth_user_groups"."id";
SELECT setval('"public"."auth_user_groups_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_user_id_seq"
OWNED BY "public"."auth_user"."id";
SELECT setval('"public"."auth_user_id_seq"', 3, true);
ALTER SEQUENCE "public"."auth_user_user_permissions_id_seq"
OWNED BY "public"."auth_user_user_permissions"."id";
SELECT setval('"public"."auth_user_user_permissions_id_seq"', 2, false);
ALTER SEQUENCE "public"."authors_genre_ids_id_seq"
OWNED BY "public"."authors_genre_ids"."id";
SELECT setval('"public"."authors_genre_ids_id_seq"', 3, true);
ALTER SEQUENCE "public"."authors_id_seq"
OWNED BY "public"."authors"."id";
SELECT setval('"public"."authors_id_seq"', 17, true);
ALTER SEQUENCE "public"."books_author_ids_id_seq"
OWNED BY "public"."books_author_ids"."id";
SELECT setval('"public"."books_author_ids_id_seq"', 2, true);
ALTER SEQUENCE "public"."books_genre_ids_id_seq"
OWNED BY "public"."books_genre_ids"."id";
SELECT setval('"public"."books_genre_ids_id_seq"', 3, true);
ALTER SEQUENCE "public"."books_id_seq"
OWNED BY "public"."books"."id";
SELECT setval('"public"."books_id_seq"', 2, true);
ALTER SEQUENCE "public"."comments_id_seq"
OWNED BY "public"."comments"."id";
SELECT setval('"public"."comments_id_seq"', 3, true);
ALTER SEQUENCE "public"."django_admin_log_id_seq"
OWNED BY "public"."django_admin_log"."id";
SELECT setval('"public"."django_admin_log_id_seq"', 4, true);
ALTER SEQUENCE "public"."django_content_type_id_seq"
OWNED BY "public"."django_content_type"."id";
SELECT setval('"public"."django_content_type_id_seq"', 14, true);
ALTER SEQUENCE "public"."django_migrations_id_seq"
OWNED BY "public"."django_migrations"."id";
SELECT setval('"public"."django_migrations_id_seq"', 18, true);
ALTER SEQUENCE "public"."editions_id_seq"
OWNED BY "public"."editions"."id";
SELECT setval('"public"."editions_id_seq"', 2, true);
ALTER SEQUENCE "public"."formalities_id_seq"
OWNED BY "public"."formalities"."id";
SELECT setval('"public"."formalities_id_seq"', 3, true);
ALTER SEQUENCE "public"."genres_id_seq"
OWNED BY "public"."genres"."id";
SELECT setval('"public"."genres_id_seq"', 3, true);
ALTER SEQUENCE "public"."languages_id_seq"
OWNED BY "public"."languages"."id";
SELECT setval('"public"."languages_id_seq"', 3, true);

-- ----------------------------
-- Indexes structure for table auth_group
-- ----------------------------
CREATE INDEX "auth_group_name_a6ea08ec_like" ON "public"."auth_group" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD CONSTRAINT "auth_group_name_key" UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD CONSTRAINT "auth_group_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id_b120cbf9" ON "public"."auth_group_permissions" USING btree (
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e" ON "public"."auth_group_permissions" USING btree (
  "permission_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_group_id_permission_id_0cd325b0_uniq" UNIQUE ("group_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id_2f476e4b" ON "public"."auth_permission" USING btree (
  "content_type_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_content_type_id_codename_01ab375a_uniq" UNIQUE ("content_type_id", "codename");

-- ----------------------------
-- Primary Key structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user
-- ----------------------------
CREATE INDEX "auth_user_username_6821ab7c_like" ON "public"."auth_user" USING btree (
  "username" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD CONSTRAINT "auth_user_username_key" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD CONSTRAINT "auth_user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_groups
-- ----------------------------
CREATE INDEX "auth_user_groups_group_id_97559544" ON "public"."auth_user_groups" USING btree (
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "auth_user_groups_user_id_6a12ed8b" ON "public"."auth_user_groups" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_user_groups
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD CONSTRAINT "auth_user_groups_user_id_group_id_94350c0c_uniq" UNIQUE ("user_id", "group_id");

-- ----------------------------
-- Primary Key structure for table auth_user_groups
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD CONSTRAINT "auth_user_groups_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_user_permissions
-- ----------------------------
CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c" ON "public"."auth_user_user_permissions" USING btree (
  "permission_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b" ON "public"."auth_user_user_permissions" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_user_user_permissions
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD CONSTRAINT "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq" UNIQUE ("user_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table auth_user_user_permissions
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD CONSTRAINT "auth_user_user_permissions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table authors
-- ----------------------------
ALTER TABLE "public"."authors" ADD CONSTRAINT "authors_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table authors_genre_ids
-- ----------------------------
CREATE INDEX "authors_genre_ids_author_id_e38ca98c" ON "public"."authors_genre_ids" USING btree (
  "author_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "authors_genre_ids_genre_id_81bd6bf8" ON "public"."authors_genre_ids" USING btree (
  "genre_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table authors_genre_ids
-- ----------------------------
ALTER TABLE "public"."authors_genre_ids" ADD CONSTRAINT "authors_genre_ids_author_id_genre_id_ac6144a7_uniq" UNIQUE ("author_id", "genre_id");

-- ----------------------------
-- Primary Key structure for table authors_genre_ids
-- ----------------------------
ALTER TABLE "public"."authors_genre_ids" ADD CONSTRAINT "authors_genre_ids_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table books
-- ----------------------------
CREATE INDEX "books_edition_id_4ca1e3f3" ON "public"."books" USING btree (
  "edition_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "books_formality_id_6f969900" ON "public"."books" USING btree (
  "formality_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "books_language_id_077fcc13" ON "public"."books" USING btree (
  "language_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table books
-- ----------------------------
ALTER TABLE "public"."books" ADD CONSTRAINT "books_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table books_author_ids
-- ----------------------------
CREATE INDEX "books_author_ids_author_id_d10f28f9" ON "public"."books_author_ids" USING btree (
  "author_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "books_author_ids_book_id_24a2aff9" ON "public"."books_author_ids" USING btree (
  "book_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table books_author_ids
-- ----------------------------
ALTER TABLE "public"."books_author_ids" ADD CONSTRAINT "books_author_ids_book_id_author_id_5b3f4b2f_uniq" UNIQUE ("book_id", "author_id");

-- ----------------------------
-- Primary Key structure for table books_author_ids
-- ----------------------------
ALTER TABLE "public"."books_author_ids" ADD CONSTRAINT "books_author_ids_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table books_genre_ids
-- ----------------------------
CREATE INDEX "books_genre_ids_book_id_9f7dbfe1" ON "public"."books_genre_ids" USING btree (
  "book_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "books_genre_ids_genre_id_991192b9" ON "public"."books_genre_ids" USING btree (
  "genre_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table books_genre_ids
-- ----------------------------
ALTER TABLE "public"."books_genre_ids" ADD CONSTRAINT "books_genre_ids_book_id_genre_id_897d90fb_uniq" UNIQUE ("book_id", "genre_id");

-- ----------------------------
-- Primary Key structure for table books_genre_ids
-- ----------------------------
ALTER TABLE "public"."books_genre_ids" ADD CONSTRAINT "books_genre_ids_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table comments
-- ----------------------------
CREATE INDEX "comments_book_id_0f40093f" ON "public"."comments" USING btree (
  "book_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "comments_user_id_b8fd0b64" ON "public"."comments" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX "django_admin_log_content_type_id_c4bce8eb" ON "public"."django_admin_log" USING btree (
  "content_type_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "django_admin_log_user_id_c564eba6" ON "public"."django_admin_log" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Checks structure for table django_admin_log
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD CONSTRAINT "django_admin_log_action_flag_check" CHECK ((action_flag >= 0));

-- ----------------------------
-- Primary Key structure for table django_admin_log
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD CONSTRAINT "django_admin_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD CONSTRAINT "django_content_type_app_label_model_76bd3d3b_uniq" UNIQUE ("app_label", "model");

-- ----------------------------
-- Primary Key structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD CONSTRAINT "django_content_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table django_migrations
-- ----------------------------
ALTER TABLE "public"."django_migrations" ADD CONSTRAINT "django_migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_expire_date_a5c62663" ON "public"."django_session" USING btree (
  "expire_date" "pg_catalog"."timestamptz_ops" ASC NULLS LAST
);
CREATE INDEX "django_session_session_key_c0390e0f_like" ON "public"."django_session" USING btree (
  "session_key" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table django_session
-- ----------------------------
ALTER TABLE "public"."django_session" ADD CONSTRAINT "django_session_pkey" PRIMARY KEY ("session_key");

-- ----------------------------
-- Primary Key structure for table editions
-- ----------------------------
ALTER TABLE "public"."editions" ADD CONSTRAINT "editions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table formalities
-- ----------------------------
ALTER TABLE "public"."formalities" ADD CONSTRAINT "formalities_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table genres
-- ----------------------------
CREATE INDEX "genres_parent_id_7a8c217d" ON "public"."genres" USING btree (
  "parent_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table genres
-- ----------------------------
ALTER TABLE "public"."genres" ADD CONSTRAINT "genres_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table languages
-- ----------------------------
ALTER TABLE "public"."languages" ADD CONSTRAINT "languages_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissio_permission_id_84c5c92e_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_group_id_b120cbf9_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_content_type_id_2f476e4b_fk_django_co" FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table auth_user_groups
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD CONSTRAINT "auth_user_groups_group_id_97559544_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_groups" ADD CONSTRAINT "auth_user_groups_user_id_6a12ed8b_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table auth_user_user_permissions
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD CONSTRAINT "auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_user_permissions" ADD CONSTRAINT "auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table authors_genre_ids
-- ----------------------------
ALTER TABLE "public"."authors_genre_ids" ADD CONSTRAINT "authors_genre_ids_author_id_e38ca98c_fk_authors_id" FOREIGN KEY ("author_id") REFERENCES "public"."authors" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."authors_genre_ids" ADD CONSTRAINT "authors_genre_ids_genre_id_81bd6bf8_fk_genres_id" FOREIGN KEY ("genre_id") REFERENCES "public"."genres" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table books
-- ----------------------------
ALTER TABLE "public"."books" ADD CONSTRAINT "books_edition_id_4ca1e3f3_fk_editions_id" FOREIGN KEY ("edition_id") REFERENCES "public"."editions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."books" ADD CONSTRAINT "books_formality_id_6f969900_fk_formalities_id" FOREIGN KEY ("formality_id") REFERENCES "public"."formalities" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."books" ADD CONSTRAINT "books_language_id_077fcc13_fk_languages_id" FOREIGN KEY ("language_id") REFERENCES "public"."languages" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table books_author_ids
-- ----------------------------
ALTER TABLE "public"."books_author_ids" ADD CONSTRAINT "books_author_ids_author_id_d10f28f9_fk_authors_id" FOREIGN KEY ("author_id") REFERENCES "public"."authors" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."books_author_ids" ADD CONSTRAINT "books_author_ids_book_id_24a2aff9_fk_books_id" FOREIGN KEY ("book_id") REFERENCES "public"."books" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table books_genre_ids
-- ----------------------------
ALTER TABLE "public"."books_genre_ids" ADD CONSTRAINT "books_genre_ids_book_id_9f7dbfe1_fk_books_id" FOREIGN KEY ("book_id") REFERENCES "public"."books" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."books_genre_ids" ADD CONSTRAINT "books_genre_ids_genre_id_991192b9_fk_genres_id" FOREIGN KEY ("genre_id") REFERENCES "public"."genres" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_book_id_0f40093f_fk_books_id" FOREIGN KEY ("book_id") REFERENCES "public"."books" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_user_id_b8fd0b64_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table django_admin_log
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD CONSTRAINT "django_admin_log_content_type_id_c4bce8eb_fk_django_co" FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."django_admin_log" ADD CONSTRAINT "django_admin_log_user_id_c564eba6_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table genres
-- ----------------------------
ALTER TABLE "public"."genres" ADD CONSTRAINT "genres_parent_id_7a8c217d_fk_genres_id" FOREIGN KEY ("parent_id") REFERENCES "public"."genres" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
