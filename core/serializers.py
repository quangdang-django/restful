from core.models import *
from rest_framework import serializers


class FormalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Formality
        fields = ('id', 'name')


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'name')


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('id', 'name')


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'name', 'dob', 'hometown', 'genre_ids')


class EditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Edition
        fields = ('id', 'name')


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'name', 'isbn', 'number_of_pages', 'publish_year', 'formality_id', 'language_id', 'author_ids',
                  'genre_ids', 'edition_id')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'book_id', 'user_id', 'content', 'publish_date')
