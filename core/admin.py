from django.contrib import admin
from core.models import *

# Register your models here.
admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Formality)
admin.site.register(Language)
admin.site.register(Comment)
admin.site.register(Edition)