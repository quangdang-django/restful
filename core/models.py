from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Formality(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'formalities'
        ordering = ['id']


class Genre(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField(blank=True, null=True)
    parent_id = models.ForeignKey('self', models.DO_NOTHING, db_column='parent_id', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'genres'
        ordering = ['id']


class Language(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'languages'
        ordering = ['id']


class Author(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(null=True)
    dob = models.IntegerField(default=0)
    hometown = models.CharField(max_length=150, blank=True, null=True)
    genre_ids = models.ManyToManyField(Genre, db_column='genre_ids')
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'authors'
        ordering = ['id']


class Edition(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'editions'
        ordering = ['id']


class Book(models.Model):
    name = models.CharField(max_length=150)
    cover_image = models.ImageField(null=True)
    isbn = models.CharField(max_length=15)
    number_of_pages = models.IntegerField(blank=True, null=True)
    publish_year = models.IntegerField(default=0)
    formality_id = models.ForeignKey(Formality, models.DO_NOTHING, db_column='formality_id', blank=True, null=True)
    language_id = models.ForeignKey(Language, models.DO_NOTHING, db_column='language_id', blank=True, null=True)
    author_ids = models.ManyToManyField(Author)
    genre_ids = models.ManyToManyField(Genre)
    edition_id = models.ForeignKey(Edition, models.DO_NOTHING, db_column='edition_id', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'books'
        ordering = ['id']


class Comment(models.Model):
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE, db_column='book_id', related_name='comment', null=True)
    user_id = models.ForeignKey(User, db_column='user_id', on_delete=models.CASCADE)
    content = models.TextField(null=True)
    publish_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.content

    class Meta:
        db_table = 'comments'
        ordering = ['publish_date']
